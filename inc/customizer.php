<?php
/**
 * openspecimen_ Theme Customizer.
 *
 * @package openspecimen
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function openspecimen_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';


	/* Setup the themes settings for customization */

	/**************** For Copyright Footer text START  *****************/
	/*Add the section in theme customizer */
	$wp_customize->add_section('footer_settings',
					array('title' => __('Footer','openspecimen'),
					'Description' => __('Modify the Footer settings')
					));

	/*Add the setting for that section  */
	$wp_customize->add_setting('footer_copyright_text',
				array('default' => '&copy; 2015. Open Specimen.'
				
				));

	/* How to modify the settings e.g color picker,  drop down, scale etc */
	$wp_customize->add_control( 'footer_copyright_text',
			array('label' => __('Modify the footer text','openspecimen'),
			'section' => 'footer_settings' ,
			'settings' => 'footer_copyright_text')
			);
	/**************** For Copyright Footer text END  *****************/

	// For Site logo
	/*Add the setting for that section  */
	$wp_customize->add_setting('site_logo',
		array('default-image' => '',
			'sanitize_callback' => 'esc_url_raw',
				));

	/* How to modify the settings e.g color picker,  drop down, scale etc */
	$wp_customize->add_control(  new WP_Customize_Image_Control( $wp_customize,
			'site_logo',
			array('label' => __('Upload Site Logo','openspecimen'),
			'section' => 'title_tagline' ,
			'settings' => 'site_logo')
			)) ;


	// for size of site logo size 
    $wp_customize->add_setting(
        'logo_size',
        array(
            'sanitize_callback' => 'absint',
            'default'           => '50',
            'transport'         => 'postMessage'
        )       
    );
    $wp_customize->add_control( 'logo_size', array(
        'type'        => 'number',
        'priority'    => 10,
        'section'     => 'title_tagline',
        'label'       => __('Logo size', 'openspecimen'),
        'description' => __('Menu-content spacing will return to normal after you save &amp; exit the Customizer', 'openspecimen'),
        'input_attrs' => array(
            'min'   => 10,
            'max'   => 300,
            'step'  => 5,
            'style' => 'margin-bottom: 15px; padding: 15px;',
        ),
    ) ); 




    // Display current version of product
    ///*Add the setting for that section  */
  	$wp_customize->add_setting('display_current_version',
				array('default' => 'Now active on version 2.0.1'
				
				));
 
	/* How to modify the settings e.g color picker,  drop down, scale etc */
 	$wp_customize->add_control( 'display_current_version',
			array('label' => __('Modify the current version text','openspecimen'),
			'section' => 'title_tagline' ,
			'settings' => 'display_current_version')
			);



    //for site description
	/*Add the setting for that section  */
    $wp_customize->add_setting( 'display_site_desc', array(
        'default'    => true
        
    ) );

    // Add control and output for select field
    $wp_customize->add_control( 'display_site_desc', array(
        'label'      => __( 'Display Description', 'openspecimen' ),
        'section'    => 'title_tagline',
        'settings'   => 'display_site_desc',
        'type'       => 'checkbox',
        'std '        => '1'
    ) ) ;

    $wp_customize->remove_control( 'display_header_text' ); 

	/*
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'layout_settings',
			array('label' => __('Modify the Theme color settings','openspecimen'),
			'section' => 'frontpage_settings' ,
			'settings' => 'layout_settings')
			));

	*/
}
add_action( 'customize_register', 'openspecimen_customize_register' );

 
/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function openspecimen_customize_preview_js() {
	wp_enqueue_script( 'openspecimen_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'openspecimen_customize_preview_js' );
