<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package openspecimen
 * Template Name: FullWidth Page
 *
**/


get_header(); ?>

	<div id="primary" class="content-area fullwidth">
		<main id="main" class="site-main" role="main">
			<header class="entry-header">
				<?php  if ( get_post_meta( get_the_ID(), 'wpcf-display-page-title', true ) == 1 ) : ?>
				 	<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<?php endif; ?>
			</header><!-- .entry-header -->

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
			<?php endwhile; // End of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->





  
<?php get_footer(); ?>
