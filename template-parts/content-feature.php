<?php
/**
 * Template part for displaying single feature.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package openspecimen
 */

?>

<article class="os_two_col feature" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="feature_display">
		<a href="<?php the_permalink(); ?>" rel="bookmark">
			<div class="entry-content">
				
				<div class="feature_image">
					<?php the_post_thumbnail(  ); ?>
				</div>
				<div class="feature_title">
					<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
				</div>
				<div class="feature_desc">
					 <?php  the_excerpt();  ?>
				</div>
				
			</div><!-- .entry-content -->
		 </a>
	</div> <!-- .feature_display -->
</article><!-- #post-## -->

