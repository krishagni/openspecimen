<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package openspecimen
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	 
			<!--  If this page is custom post type dont print the meta fields.-->
			<?php if(  is_singular( array('page', 'attachment', 'post') ) ) : ?>
				 <div class="entry-meta">
					<?php openspecimen_posted_on(); ?>
				</div><!-- .entry-meta -->
			<?php endif; ?>

		
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'openspecimen' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php openspecimen_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

