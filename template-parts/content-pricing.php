<?php
/**
 * Template part for displaying Pricing posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package openspecimen
 */

?>

<article class="price" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="price_display">
		
			<div class="entry-content">
				
			 	<div class="price-top">
			 		<div class="price-title">
			 			<?php  echo types_render_field("price-title"); ?>
			 		</div>
			 		<div class="price-border"></div>
					<?php  echo types_render_field("price-desc"); ?>
			 	</div>
				<div class="price-bottom">
					<div class="price-label"> GET IT FOR <BR></div>
					<div class="price-pricevalue">
						$ <?php echo number_format(types_render_field("price-price")); ?>
					</div> 
					
					<div class="price-label"> per <?php echo strtolower(types_render_field("price-per")); ?></div>
					<div class="price-features">
						<?php 
						 
							print_r(types_render_field("price-feature",array("output" => "html")));
						 
						//echo types_render_field("price-feature"); 
							?>
					</div>
					
					 
					<a href="<?php  types_render_field("price-button-link");?>" rel="bookmark"> Get Started
					 </a>	
				</div>
			</div><!-- .entry-content -->
	</div> <!-- .feature_display -->
</article><!-- #post-## -->

