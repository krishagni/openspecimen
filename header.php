<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package openspecimen
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<meta property="fb:app_id" content="1453490271572658" />


	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'openspecimen' ); ?></a>

		<header id="masthead" class="site-header" role="banner">
			<div class="header-right-menu">
				<div class="display_current_version"><p> <?php echo get_theme_mod('display_current_version','Now active on version 2.0.1'); ?> </p> </div>
				<div class="header_social_menu"><p></p>	<?php dynamic_sidebar( 'Header social area' ); ?> </div>
			</div>
			
			<div class="site-branding">
				<a href="<?php echo esc_url( home_url( '/' ) );?>" title="<?php echo bloginfo( 'name' )?>"  >
					<?php if ( get_theme_mod('site_logo') ) : ?>
						<div class="site-logo">
							<!-- If logo size if not defined in customizer - take 50px 	-->
							<img  width="<?php echo get_theme_mod('logo_size','50'); ?>px"  
							src=" <?php  echo esc_url(get_theme_mod('site_logo'));?>" alt="<?php echo bloginfo('name'); ?>" />  
						</div> 
					<?php  endif; ?>
					<div class="site-name">
						<h1><?php echo  bloginfo( 'name' );?> </h1>
					</div>
					<?php if ( get_theme_mod('display_site_desc') == true) : ?>
						<div class="site-desc">
							<p><?php bloginfo( 'description' ); ?></p>
						</div>
					<?php  endif; ?>
				</a> 
			</div><!-- .site-branding -->
			<nav id="site-navigation" class="main-navigation" role="navigation" >
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'openspecimen' ); ?></button>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</nav><!-- #site-navigation -->
		</header><!-- #masthead -->

		<div id="content" class="site-content">
