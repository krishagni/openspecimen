<?php
/**
 * openspecimen_ functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package openspecimen
 */

if ( ! function_exists( 'openspecimen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function openspecimen_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on openspecimen_, use a find and replace
	 * to change 'openspecimen' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'openspecimen', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'openspecimen' ),
		) );
	register_nav_menus( array(
		'footer' => esc_html__( 'Footer Menu', 'openspecimen' ),
		) );
	register_nav_menus( array(
		'categories' => esc_html__( 'Categories', 'openspecimen' ),
		) );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
		) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'openspecimen_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
		) ) );

	


}
endif; // openspecimen_setup
add_action( 'after_setup_theme', 'openspecimen_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function openspecimen_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'openspecimen_content_width', 640 );
}
add_action( 'after_setup_theme', 'openspecimen_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function openspecimen_widgets_init() {

	//Register Left Sidebar 
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'openspecimen' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
		) );

		//Register social icons - menu bar for header 
	register_sidebar( array(
		'name' =>'header social area',
		'id' => 'header_social_area',
		'before_widget' => '<div class="header_social_area">',
		'after_widget' => '</div>',
		'before_title' => '',
		'after_title' => '',
		) );
	// Register 3 footer sidebars.
	register_sidebar( array(
		'name' => 'Footer Sidebar 1',
		'id' => 'footer-sidebar-1',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
	register_sidebar( array(
		'name' => 'Footer Sidebar 2',
		'id' => 'footer-sidebar-2',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
	register_sidebar( array(
		'name' => 'Footer Sidebar 3',
		'id' => 'footer-sidebar-3',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
	register_sidebar( array(
		'name' => 'Footer Sidebar 4',
		'id' => 'footer-sidebar-4',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
}
add_action( 'widgets_init', 'openspecimen_widgets_init' );





/**
 * Enqueue scripts and styles.
 */
function openspecimen_scripts() {
	
	/* Load Google Fonts */
	$query_args = array(
		'family' => 'PT+Sans:400,400italic,700|Arvo:400,400italic,700,700italic|Roboto:400,500,700',
		'subset' => 'latin,latin-ext'
		);
	wp_register_style( 'google_fonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );

	// Load Theme Stylesheet.
	wp_enqueue_style( 'openspecimen-style', get_stylesheet_uri() );

	/*Insert Jquery from Google */
	wp_deregister_script('jquery');
	wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", true, null);
	
	//Load Theme Js
	wp_enqueue_script( 'openspecimen-theme-js', get_template_directory_uri() . '/js/openspecimen.js', array('jquery'), '20151115', true );

	//Load Menu navigation js 
	wp_enqueue_script( 'openspecimen-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'openspecimen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'openspecimen_scripts' );



//Dynamic styles - add it styles to the already enqueued style.css
function isws_custom_styles($custom) {
	global $post;

	// If page bg is set then get the value and display it.
	$background_img = get_post_meta( get_the_ID(), 'wpcf-pg-head-image', true );

	if ( $background_img ) {
		$custom = ".page-id-" . $post->ID . " .site-header". " {   background-image: url('" . esc_url($background_img) . "') !important; background-attachment: fixed !important; background-repeat: no-repeat !important; background-size: cover;}"."\n";
	}  

    //If Menu font color - has been set then change menu color accordingly
	$menu_font_color = get_post_meta( get_the_ID(), 'wpcf-menu-font-color', true );
	if ( $menu_font_color ) {
		$custom = ".site-name h1, #masthead a, .header-right-menu p, .main-navigation li:after, .site-desc p {  color:". esc_url($menu_font_color)  ."\n";
		
	}  


	wp_add_inline_style( 'openspecimen-style', $custom );	
}
add_action( 'wp_enqueue_scripts', 'isws_custom_styles' );

 

function create_user_from_registration($cfdata) {

	//Check if we have the right version of contact form 7 plugin
	if (!isset($cfdata->posted_data) && class_exists('WPCF7_Submission')) {
        // Contact Form 7 version 3.9 removed $cfdata->posted_data and now
        // we have to retrieve it from an API
		$submission = WPCF7_Submission::get_instance();
		if ($submission) {
			$formdata = $submission->get_posted_data();
		}
	} elseif (isset($cfdata->posted_data)) {
        // For pre-3.9 versions of Contact Form 7
		$formdata = $cfdata->posted_data;
	} else {
        // We can't retrieve the form data
		return $cfdata;
	}
	    // Check if this is the user registration form we need to process 
	if ( $cfdata->title() == 'User Registration') {

	    	// Get basic details
		$password = wp_generate_password( 12, false );
		$email = $formdata['user-email'];
		$lastname = $formdata['last-name'];
		$firstname  = $formdata['first-name'];
		$username  = $formdata['first-name'];
	        // Construct a username from the user's name
		//$username = strtolower(str_replace(' ', '', $name));
		//$name_parts = explode(' ',$name);
	        //create the array to insert into table - if the email id doesnt exist
		if ( !email_exists( $email ) ) {
	            // Find an unused username
			$username_tocheck = $username;
			$i = 1;
			while ( username_exists( $username_tocheck ) ) {
				$username_tocheck = $username . $i++;
			}
			$username = $username_tocheck;
	            // Create the user
			$userdata = array(
				'user_login' => $username,
				'user_pass' => $password,
				'user_email' => $email,
				'nickname' => $firstname,
				'display_name' => $firstname.' '.$lastname,
				'first_name' => $firstname,
				'last_name' => $lastname,
				'role' => 'subscriber'
				);
			$user_id = wp_insert_user( $userdata );
			if ( !is_wp_error($user_id) ) {
	                // Email login details to user
				$headers = array('Content-Type: text/html; charset=UTF-8');
				$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
				$message = "<div style='font-size:15px;padding:40px;font-family:arial;line-height:27px;color:#7f7f7f;text-align: justify;background:#f6f8f1;'>";
				$message .= sprintf(__("Welcome! %s, Your login details are as follows:"), $first_name) . "<br>";
				$message .= sprintf(__('Username: %s'), $username) . "<br>";
				$message .= sprintf(__('Password: %s'), $password) . "<br>";
				$message .= wp_login_url() . "<br>";
 
				$message .= "<br><strong>- OpenSpecimen Administrator</strong><br>OpenSpecimen | admin@openspecimen.org <br></div>";
 				_log( "[". $message."] [". $blogname."]");
				wp_mail($email, sprintf(__('[%s] Your username and password'), $blogname), $message,$headers);
			}
		}
		else {
	        	//Email id exisit in the database - return a errror

			return $cfdata;

		}
	}
	return $cfdata;
}
add_action('wpcf7_before_send_mail', 'create_user_from_registration', 2);

 
function custom_email_validation_filter($result,$tag){
    $type = $tag['type'];
    $name = $tag['name'];
    if($name == 'user-email'){ // Only apply to fields with the form field name of "form-email"
        $the_value = $_POST[$name];
        if(email_exists($the_value)){ // https://codex.wordpress.org/Function_Reference/email_exists
            $result['valid'] = false;
            $result->invalidate( $tag, 'Email address already exists, please choose another.' );
        }
    }
    return $result;
}
add_filter('wpcf7_validate_email','custom_email_validation_filter', 10, 2); // Email field
add_filter('wpcf7_validate_email*', 'custom_email_validation_filter', 10, 2); // Req. Email field
 



/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';




// Setting up a generic function to print debug messages to debug log
if(!function_exists('_log')){
  function _log( $message ) {
    if( WP_DEBUG === true ){
      if( is_array( $message ) || is_object( $message ) ){
        error_log( print_r( $message, true ) );
      } else {
        error_log( $message );
      }
    }
  }
}






