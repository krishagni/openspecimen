<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package openspecimen
 */

?>

	</div><!-- #content -->
<!--
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<a href="#"> <?php echo get_theme_mod( 'footer_copyright_text' ); ?> </a>
			<span class="sep"> | </span>

			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'openspecimen' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'openspecimen' ), 'WordPress' ); ?></a>
			-->			<!--<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'openspecimen' ), 'openspecimen', '<a href="http://underscores.me/" rel="designer">Underscores.me</a>' ); ?>
		</div> --> <!-- .site-info -->


<footer id="colophon" class="site-footer" role="contentinfo">
		<div id="footer-sidebar" class="secondary">
			<div id="footer-sidebar1">
				<?php if(is_active_sidebar('footer-sidebar-1')){
					dynamic_sidebar('footer-sidebar-1');
				} ?>
			</div>
			<div id="footer-sidebar2">
				<?php if(is_active_sidebar('footer-sidebar-2')){
					dynamic_sidebar('footer-sidebar-2');
				} ?>
			</div>
			<div id="footer-sidebar3">
				<?php if(is_active_sidebar('footer-sidebar-3')){
					dynamic_sidebar('footer-sidebar-3');
				} ?>
			</div>
			<div id="footer-sidebar4">
				<?php if(is_active_sidebar('footer-sidebar-4')){
					dynamic_sidebar('footer-sidebar-4');
				} ?>
			</div>
	</div>



		
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
